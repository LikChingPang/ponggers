using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

[RequireComponent(typeof(Button))]
public class WaitingMenuReturnButton : MonoBehaviour
{
    [SerializeField] GameObject lobbyMenu, waitMenu;

    public void LeaveRoom()
    {
        // Leave the current room
        PhotonNetwork.LeaveRoom();

        // Hide the waiting menu
        waitMenu.SetActive(false);

        // Show the lobby menu
        lobbyMenu.SetActive(true);
    }
}
