using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the glowing effect for the start game button in the waiting room
/// </summary>

[RequireComponent(typeof(Image))]
public class StartGameButtonGlowingEffect : MonoBehaviour
{
    [SerializeField] float aplhaChangeRatio = 0.001f;
    [SerializeField] bool isGlowing;

    Image effectImage;

    void Start()
    {
        // Initialize
        effectImage = gameObject.GetComponent<Image>();
    }

    void FixedUpdate()
    {
        // If the image is not glowing bright
        if (isGlowing == false)
        {
            float newAlpha = effectImage.color.a - aplhaChangeRatio;
            // Reduce the alpha of the image by aplhaChangeRatio
            effectImage.color = new Color(effectImage.color.r, effectImage.color.g, effectImage.color.b, newAlpha);
        }
        // If the star is glowing bright
        else
        {
            float newAlpha = effectImage.color.a + aplhaChangeRatio;
            // Increase the alpha of the image by aplhaChangeRatio
            effectImage.color = new Color(effectImage.color.r, effectImage.color.g, effectImage.color.b, newAlpha);
        }

        if (effectImage.color.a <= 0)
        {
            isGlowing = true;
        }
        else if (effectImage.color.a >= 1)
        {
            isGlowing = false;
        }
    }
}
