using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class StartGameButton : MonoBehaviourPunCallbacks
{
    public void StartGame()
    {
        // Load scene 1 for everyone in the lobby, which is the game scene
        PhotonNetwork.LoadLevel(1);
    }
}
