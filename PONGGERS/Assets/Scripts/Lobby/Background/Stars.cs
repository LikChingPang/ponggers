using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Stars : MonoBehaviour
{
    [SerializeField] float aplhaChangeRatio = 0.001f;
    [SerializeField] bool isGlowing;

    SpriteRenderer starColor;

    void Start()
    {
        // Initialize
        starColor = gameObject.GetComponent<SpriteRenderer>();

        // Set the color to white
        starColor.color = new Color(starColor.color.r, starColor.color.g, starColor.color.b, starColor.color.a);
    }

    void FixedUpdate()
    {
        // If the star is not glowing bright
        if (isGlowing == false)
        {
            float newAlpha = starColor.color.a - aplhaChangeRatio;
            // Reduce the alpha of the star by aplhaChangeRatio
            starColor.color = new Color(starColor.color.r, starColor.color.g, starColor.color.b, newAlpha);
        }
        // If the star is glowing bright
        else
        {
            float newAlpha = starColor.color.a + aplhaChangeRatio;
            // Increase the alpha of the star by aplhaChangeRatio
            starColor.color = new Color(starColor.color.r, starColor.color.g, starColor.color.b, newAlpha);
        }

        if (starColor.color.a <= 0)
        {
            isGlowing = true;
        }
        else if (starColor.color.a >= 1)
        {
            isGlowing = false;
        }
    }
}
