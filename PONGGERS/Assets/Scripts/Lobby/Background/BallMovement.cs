using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for the ball's movement in the starting scene
/// </summary>

[RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D))]
public class BallMovement : MonoBehaviour
{
    [SerializeField] float xForce = 50;

    Rigidbody2D ballRigidbody;
    int direction;

    void Start()
    {
        // Initialize
        direction = -1;

        // Set ball's alpha to 1
        Color ballColor = gameObject.GetComponent<SpriteRenderer>().color;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(ballColor.r, ballColor.g, ballColor.b, 1);
    }

    private void OnEnable()
    {
        MoveBall();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If the ball hits a vertial bar
        if (collision.transform.GetComponent<VertialBars>() != null)
        {
            // Change the X direction of the ball to the direct opposite
            ballRigidbody.velocity = new Vector2(ballRigidbody.velocity.x * direction, ballRigidbody.velocity.y);
        }

        // If the ball hits a horizontal bar
        if (collision.transform.GetComponent<HorizontalBars>() != null)
        {
            // Change the Y direction of the ball to the direct opposite
            ballRigidbody.velocity = new Vector2(ballRigidbody.velocity.x, ballRigidbody.velocity.y * direction);
        }
    }

    private void MoveBall()
    {
        // Initialize
        ballRigidbody = gameObject.GetComponent<Rigidbody2D>();
        int startingXDirection = -1, startingYDirection = -1;

        // Random a direction for x
        int randomizer = Random.Range(0, 2);

        if (randomizer == 0)
        {
            startingXDirection = -1;
        }
        else
        {
            startingXDirection = 1;
        }

        // Random a direction for y
        randomizer = Random.Range(0, 2);

        if (randomizer == 0)
        {
            startingYDirection = -1;
        }
        else
        {
            startingYDirection = 1;
        }

        // Construct the initial force
        Vector2 initForce = new Vector2(xForce * startingXDirection, Random.Range(30, 60) * startingYDirection);

        // Add the initial force to the ball
        ballRigidbody.AddForce(initForce);
    }
}
