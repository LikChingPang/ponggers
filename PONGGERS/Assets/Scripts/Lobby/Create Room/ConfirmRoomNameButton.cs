using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using Photon.Realtime;

/// <summary>
/// This script is responsible for the confirm room name button in the create room menu
/// </summary>

[RequireComponent(typeof(Button))]
public class ConfirmRoomNameButton : MonoBehaviourPunCallbacks
{
    [SerializeField] PhotonManager photonManager;
    [SerializeField] TMP_InputField roomNameInputField;
    [SerializeField] TMP_Text warningText;
    [SerializeField] string emtpyRoomNameWarningText = "Please enter a room name", ExistRoomNameWarningText = "The room name already exists";
    [SerializeField] Animation warningTextAnimation;
    [SerializeField] GameObject createRoomMenu;

    bool isExist = false;

    void Start()
    {
        // Initialize
        isExist = false;
    }

    public void Confirm()
    {
        // Reset isExist
        isExist = false;

        // Check all the rooms' name
        foreach (var room in photonManager.rooms)
        {
            // If they are the same
            if (roomNameInputField.text == room.Name)
            {
                // Set isExist to true
                isExist = true;
                // Stop looping
                break;
            }
        }

        // If the input field is empty
        if (string.IsNullOrEmpty(roomNameInputField.text))
        {
            // Change the input field text to ask the player to input a room name
            warningText.text = emtpyRoomNameWarningText;

            // Display warning message and play the waring message animation
            warningTextAnimation.Play();
        }
        // If the name of the room already exists in the room list
        else if (isExist == true)
        {
            // Change the input field text to ask the player to input a non existing room name
            warningText.text = ExistRoomNameWarningText;

            // Display warning message and play the waring message animation
            warningTextAnimation.Play();
        }
        // If the name is valid
        else
        {
            // Create the room for the player
            CreateRoom();
        }
    }

    private void CreateRoom()
    {
        // Setup restrictions for the room
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 2 };
        // Create the room with the name user input and the restrictions
        PhotonNetwork.CreateRoom(roomNameInputField.text, roomOptions);
        createRoomMenu.SetActive(false);
    }
}
