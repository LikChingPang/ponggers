using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the behaviour of the return button of the create room menu
/// </summary>

[RequireComponent(typeof(Button))] 
public class CreateRoomReturnButton : MonoBehaviour
{
    [SerializeField] GameObject createRoomMenu, lobbyMenu;

    public void ReturnToLobby()
    {
        createRoomMenu.SetActive(false);
        lobbyMenu.SetActive(true);
    }
}
