using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RoomPrefeb : MonoBehaviour
{
    [SerializeField] TMP_Text roomNameText;

    RoomInfo info;

    public void Setup(RoomInfo roomInfo)
    {
        info = roomInfo;
        roomNameText.text = roomInfo.Name;
    }

    public void JoinRoom()
    {
        PhotonManager.instance.JoinRoom(info);
    }
}
