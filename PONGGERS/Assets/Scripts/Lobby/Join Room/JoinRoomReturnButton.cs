using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the behaviour of the return button of the join room menu
/// </summary>

[RequireComponent(typeof(Button))]

public class JoinRoomReturnButton : MonoBehaviour
{
    [SerializeField] GameObject joinRoomMenu, lobbyMenu;

    public void ReturnToLobby()
    {
        joinRoomMenu.SetActive(false);
        lobbyMenu.SetActive(true);
    }
}
