using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

/// <summary>
/// This script is responsible for handling the player input for their nickname
/// </summary>

[RequireComponent(typeof(Button))]
public class EnterGameButton : MonoBehaviour
{
    [SerializeField] TMP_InputField nameInputField;
    [SerializeField] GameObject playerNameTakingCanvas, lobbyCamvas;
    [SerializeField] TMP_Text playerNameText;

    void Start()
    {
        // Reset the player name
        PlayerPrefs.SetString("PlayerName", "");
    }

    public void EnterGame()
    {
        // If the name input field is not empty
        if (string.IsNullOrEmpty(nameInputField.text) == false)
        {
            // Save player's nickname to playerprefs
            PlayerPrefs.SetString("PlayerName", nameInputField.text);
            // Save player's nickname to photonnetwork
            PhotonNetwork.NickName = nameInputField.text;

            // Change Menu to Lobby Menu
            playerNameTakingCanvas.SetActive(false);
            lobbyCamvas.SetActive(true);

            // Set playerNameText's text to whatever we saved as the player name
            playerNameText.text = "Player: " + PlayerPrefs.GetString("PlayerName");
        }
    }
}
