using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the behaviour of the create room button in the lobby menu
/// </summary>

[RequireComponent(typeof(Button))]
public class LobbyCreateRoomButton : MonoBehaviour
{
    [SerializeField] GameObject createRoomMenu, lobbyMenu;

    public void ToCreateRoomMenu()
    {
        lobbyMenu.SetActive(false);
        createRoomMenu.SetActive(true);
    }
}
