using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the behaviour of the create room button in the lobby menu
/// </summary>

[RequireComponent(typeof(Button))]

public class LobbyJoinRoomButton : MonoBehaviour
{
    [SerializeField] GameObject joinRoomMenu, lobbyMenu;

    public void ToJoinRoomMenu()
    {
        lobbyMenu.SetActive(false);
        joinRoomMenu.SetActive(true);
    }
}
