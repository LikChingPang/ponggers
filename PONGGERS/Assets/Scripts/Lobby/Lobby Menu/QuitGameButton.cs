using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the quit game button's behaviour in the game
/// </summary>

[RequireComponent(typeof(Button))]
public class QuitGameButton : MonoBehaviour
{
    public void Quit()
    {
        // End the application
        Application.Quit();
    }
}
