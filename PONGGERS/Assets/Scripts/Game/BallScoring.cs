using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class BallScoring : MonoBehaviourPun
{
    [SerializeField] TMP_Text player1ScoreText, player2ScoreText, winnerText;

    private void OnTriggerExit2D(Collider2D collision)
    {
        // If the ball hits a goal
        if (collision.transform.GetComponent<Goal>() != null)
        {
            // If the ball is on the right hand side of the screen
            if (transform.position.x >= 0)
            {
                // Increare player 1's score
                player1ScoreText.text = (int.Parse(player1ScoreText.text) + 1).ToString();

                // If player 1 has 5 points or more
                if (int.Parse(player1ScoreText.text) >= 5)
                {
                    // Player 1 wins
                    // Disable the ballMovement Script
                    gameObject.GetComponent<BallMovement>().enabled = false;

                    // Set ball's alpha to 0
                    Color ballColor = gameObject.GetComponent<SpriteRenderer>().color;
                    gameObject.GetComponent<SpriteRenderer>().color = new Color(ballColor.r, ballColor.g, ballColor.b, 0);

                    // Set the win message
                    winnerText.text = PhotonNetwork.PlayerList[0].NickName + " wins!";

                    // Show the win message
                    winnerText.gameObject.SetActive(true);

                    // Invoke the leave room method in 5 seconds
                    Invoke("CloseRoom", 5);
                }
            }
            // If the ball is on the left hand side of the screen
            else
            {
                // Increare player 2's score
                player2ScoreText.text = (int.Parse(player2ScoreText.text) + 1).ToString();

                // If player 2 has 5 points or more
                if (int.Parse(player2ScoreText.text) >= 5)
                {
                    // Player 2 wins
                    // Disable the ballMovement Script
                    gameObject.GetComponent<BallMovement>().enabled = false;

                    // Set ball's alpha to 0
                    Color ballColor = gameObject.GetComponent<SpriteRenderer>().color;
                    gameObject.GetComponent<SpriteRenderer>().color = new Color(ballColor.r, ballColor.g, ballColor.b, 0);

                    // Set the win message
                    winnerText.text = PhotonNetwork.PlayerList[1].NickName + " wins!";

                    // Show the win message
                    winnerText.gameObject.SetActive(true);

                    // Invoke the leave room method in 5 seconds
                    Invoke("CloseRoom", 5);
                }
            }

            // Set itself to inactive to tell the gameManager that there is a score
            gameObject.SetActive(false);
        }
    }

    private void CloseRoom()
    {
        // Leave the room
        PhotonNetwork.LeaveRoom();
    }
}
