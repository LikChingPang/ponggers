using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class PhotonManager : MonoBehaviourPunCallbacks
{
    public static PhotonManager instance;

    public List<RoomInfo> rooms;

    [SerializeField] GameObject waitMenu, createRoomMenu, joinRoomMenu, lobbyMenu, waitingForPlayerImage, startGame;
    [SerializeField] TMP_Text waitMenuRoomNameText, player1NameText, player2NameText;
    [SerializeField] Transform roomContentList;
    [SerializeField] GameObject roomPrefebs;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnJoinedRoom()
    {
        // Set the wait menu to true
        waitMenu.SetActive(true);

        // Hide the unwanted menus
        createRoomMenu.SetActive(false);
        joinRoomMenu.SetActive(false);

        // Set the room name in the wait menu
        waitMenuRoomNameText.text = $"Room: {PhotonNetwork.CurrentRoom.Name}";

        // If the user is the host
        if (PhotonNetwork.IsMasterClient)
        {
            // Put his name into P1
            player1NameText.text = PhotonNetwork.NickName;
        }
        // If not
        else
        {
            // Get the player list
            Player[] players = PhotonNetwork.PlayerList;

            // Put master client's name into P1
            player1NameText.text = players[0].NickName;

            // Put his name into P2
            player2NameText.text = players[1].NickName;
        }
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to create a room");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        rooms = roomList;

        foreach(Transform _transform in roomContentList)
        {
            Destroy(_transform.gameObject);
        }
        for (int i = 0; i < roomList.Count; i++)
        {
            Instantiate(roomPrefebs, roomContentList).GetComponent<RoomPrefeb>().Setup(roomList[i]);
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        // Get the player list
        Player[] players = PhotonNetwork.PlayerList;

        // Put master client's name into P1
        player1NameText.text = players[0].NickName;

        // Put his name into P2
        player2NameText.text = players[1].NickName;

        // If there are 2 players and the user is the master client
        if (players.Length == 2 && PhotonNetwork.IsMasterClient == true)
        {
            // Hide the waiting for player image
            waitingForPlayerImage.SetActive(false);

            // Show the start game button
            startGame.SetActive(true);
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        // Put the user's nickname into P1
        player1NameText.text = PhotonNetwork.NickName;

        // Reset the P2 name text
        player2NameText.text = "";

        // Hide the start game button
        startGame.SetActive(false);

        // Show the waiting for player image
        waitingForPlayerImage.SetActive(true);
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
    }
}
