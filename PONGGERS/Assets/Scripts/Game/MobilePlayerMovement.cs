using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for player's movement on Android / iOS
/// </summary>

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]

public class MobilePlayerMovement : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] float movespeed = 7f, bouncebackForce = 2.5f;

    Transform myPlayer;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        myPlayer = gameManager.myPlayer.transform;
    }

    void Update()
    {
        // If there is only 1 touch
        if (Input.touchCount == 1)
        {
            foreach (Touch touch in Input.touches)
            {
                // If the touch is in the top half of the screen
                if (touch.position.y >= Screen.height / 2)
                {
                    // Move up
                    myPlayer.Translate(new Vector2(0, movespeed * Time.deltaTime));
                }
                // If the touch is in the bottom half of the screen
                else if (touch.position.y < Screen.height / 2)
                {
                    // Move down
                    myPlayer.Translate(new Vector2(0, -movespeed * Time.deltaTime));
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If the player hit a boarder
        if (collision.transform.GetComponent<HorizontalBars>() != null)
        {
            // If the player is on the top half the of screen
            if (transform.position.y >= 0)
            {
                // Bounce the player back
                transform.Translate(new Vector2(0, -movespeed * Time.deltaTime * bouncebackForce));
            }

            // If the player is on the bottom half the of screen
            else if (transform.position.y < 0)
            {
                // Bounce the player back
                transform.Translate(new Vector2(0, movespeed * Time.deltaTime * bouncebackForce));
            }
        }
    }

    // For safety
    private void OnTriggerStay(Collider other)
    {
        // If the player hit a boarder
        if (other.transform.GetComponent<HorizontalBars>() != null)
        {
            // If the player is on the top half the of screen
            if (transform.position.y >= 0)
            {
                // Bounce the player back
                transform.Translate(new Vector2(0, -movespeed * Time.deltaTime * bouncebackForce));
            }

            // If the player is on the bottom half the of screen
            else if (transform.position.y < 0)
            {
                // Bounce the player back
                transform.Translate(new Vector2(0, movespeed * Time.deltaTime * bouncebackForce));
            }
        }
    }
}
