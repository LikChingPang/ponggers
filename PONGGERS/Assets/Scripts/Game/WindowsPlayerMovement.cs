using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for player's movement on WIndows / OSX / Linux / WebGL
/// </summary>

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class WindowsPlayerMovement : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] float movespeed = 7f, bouncebackForce = 2.5f;

    Transform myPlayer;
    KeyCode upKey = KeyCode.W, downKey = KeyCode.S;

    void Start()
    {
        // Initialize
        gameManager = FindObjectOfType<GameManager>();
        myPlayer = gameManager.myPlayer.transform;
    }

    void Update()
    {
        if (Input.GetKey(upKey))
        {
            // Move up
            myPlayer.Translate(new Vector2(0, movespeed * Time.deltaTime));
        }

        if (Input.GetKey(downKey))
        {
            // Move down
            myPlayer.Translate(new Vector2(0, -movespeed * Time.deltaTime));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If the player hit a boarder
        if (collision.transform.GetComponent<HorizontalBars>() != null)
        {
            // If the player is on the top half the of screen
            if (myPlayer.position.y >= 0)
            {
                // Bounce the player back
                myPlayer.Translate(new Vector2(0, -movespeed * Time.deltaTime * bouncebackForce));
            }

            // If the player is on the bottom half the of screen
            else if (myPlayer.position.y < 0)
            {
                // Bounce the player back
                myPlayer.Translate(new Vector2(0, movespeed * Time.deltaTime * bouncebackForce));
            }
        }
    }

    // For safety
    private void OnTriggerStay(Collider other)
    {
        // If the player hit a boarder
        if (other.transform.GetComponent<HorizontalBars>() != null)
        {
            // If the player is on the top half the of screen
            if (gameManager.myPlayer.transform.position.y >= 0)
            {
                // Bounce the player back
                gameManager.myPlayer.transform.Translate(new Vector2(0, -movespeed * Time.deltaTime * bouncebackForce));
            }

            // If the player is on the bottom half the of screen
            else if (gameManager.myPlayer.transform.position.y < 0)
            {
                // Bounce the player back
                gameManager.myPlayer.transform.Translate(new Vector2(0, movespeed * Time.deltaTime * bouncebackForce));
            }
        }
    }
}
