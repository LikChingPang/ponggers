using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

/// <summary>
/// This script is responsible for managing the game
/// </summary>

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField] GameObject player1, player2, ball;
    [SerializeField] Transform ballSpawnPoint;
    [SerializeField] TMP_Text player1NameText, player2NameText;

    [HideInInspector] public GameObject myPlayer;

    void Awake()
    {
        // Set the nicknames in the conners
        // Get the player list
        Player[] players = PhotonNetwork.PlayerList;

        // For safety
        // If there are 2 players
        if (players.Length == 2)
        {
            // Put master client's name into P1
            player1NameText.text = players[0].NickName;

            // Put his name into P2
            player2NameText.text = players[1].NickName;
        }

        // If the player is the host of the game
        if (PhotonNetwork.IsMasterClient == true)
        {
            // Give the control of P1 to the user
            myPlayer = player1;
        }
        else
        {
            // Give the control of P2 to the user
            myPlayer = player2;

            // For safety
            if (players.Length == 2)
            {
                player2.GetComponent<PhotonView>().TransferOwnership(players[1]);
            }
        }

        // If player is on Windows / OSX / Linux / WebGL
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer ||
            Application.platform == RuntimePlatform.LinuxEditor || Application.platform == RuntimePlatform.LinuxPlayer ||
            Application.platform == RuntimePlatform.WebGLPlayer)
        {
            myPlayer.AddComponent<WindowsPlayerMovement>();
        }
        // If player is on android / ios
        else if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            myPlayer.AddComponent<MobilePlayerMovement>();
        }
    }

    void FixedUpdate()
    {
        // If the ball is not active
        if (ball.activeSelf == false)
        {
            // Set the ball to active in order to restart the game
            ball.SetActive(true);

            // Pause ball's movement
            ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            // Set the ball back to the original position
            ball.transform.position = ballSpawnPoint.position;
        }
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel(0);
    }
}
